
import Home from './home'
import NotFound from './404'

export {
    Home,
    NotFound,
}
