import { Response, Request } from 'express'
import fs from 'fs'
import { Logger } from '..'

import { Responser } from '..'
import { Axios, Color } from '..'


class Home {

    async welcome(req: Request, res: Response) {
        //Responser.good('Api Tester Server is Running, Enjoy :)', res)


        let files: any = fs.readFileSync('./files/log_api.json', 'utf8')
        try {
            files = JSON.parse(files)
        } catch {
            files = []
        }

        files = files.splice(500, 100)

        type loges = {
            method: string,
            query: string,
            body: {},
            exec_at: Date
        }


        //const baseUrl = 'https://connecter-staging.helloyuna.io'
        //const token = '$2b$10$.C4V4sDyL0o58IPOmzFGluwHuf6TQ/WEVQgqrbdb1S4bdZoW1RP2q'
        const baseUrl = 'https://connecter-dev.helloyuna.io'
        const token = '$2b$10$BVUXYxHvWO3IvU8/zEa0k.vffVC656ejPePQ/gwAPRCcsZLwGMSwe'
        //const resultsPath = './files/result.json'
        //fs.writeFileSync('./files/result.json', JSON.stringify([]), 'utf8')
        //const results = JSON.parse(fs.readFileSync(resultsPath, 'utf8'))
        Logger.log('Count total ' + files.length + ' logs')
        
        /*
        //--------------------- ASYNC
        const test = await Promise.allSettled(files.map((file: any, i: number) => {
            const path = Object.keys(file)[0]
            const log = file[path] as loges
            const url = baseUrl + path + log.query
            //if (i <= 10) {
            return new Promise((resolve, reject) => {
                Axios.method(log.method, url, log.body, token)
                    .then((res: any) => {
                        Logger.log((i + 1) + '> ' + log.method + path + ' --------> SUCCESS...', Color.green)
                        resolve('')
                    })
                    .catch((err: any) => {
                        Logger.log((i + 1) + '> ' + log.method + path + ' -------- ERROR...', Color.red)
                        try {
                            Logger.log(err.response.data)
                        } catch {
                            Logger.log(err.response)
                        }
                        //results.push(file)
                        reject('')
                    })
            })
        })).then(res => res).catch(err => err)

        //fs.writeFileSync(resultsPath, JSON.stringify(results), 'utf8')
        


        const result = test.reduce((first: { success: number, error: number }, item: any, index: number, array: []) => {
            if (item.status === 'fulfilled') {
                return {
                    success: first.success + 1,
                    error: first.error + 0
                }
            }
            if (item.status === 'rejected') {
                return {
                    success: first.success + 0,
                    error: first.error + 1
                }
            }
        }, {
            success: 0,
            error: 0
        })


        Logger.log({ ...result, total: files.length })
        //--------------------- ASYNC
        */

        //--------------------- LOOPING
        for (let i = 0; i <= files.length - 1; i++) {
            const file = files[i]
            const path = Object.keys(file)[0]
            const log = file[path] as loges
            const url = baseUrl + path + log.query

            await Axios.method(log.method, url, log.body, token)
                .then((res: any) => {
                    Logger.log((i + 1) + '> ' + log.method + path + ' --------> SUCCESS...', Color.green)
                })
                .catch((err: any) => {
                    Logger.log((i + 1) + '> ' + log.method + path + ' -------- ERROR...', Color.red)
                    try {
                        Logger.log(err.response.data)
                    } catch {
                        Logger.log(err.response)
                    }
                })
        }

        //--------------------- LOOPING
       


        Logger.log('Test Finished..........................', Color.blue)


        Responser.good('Api Tester Server is Running, Enjoy :)', res)
    }

}

export default new Home()
