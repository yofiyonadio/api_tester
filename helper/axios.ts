
import axios from 'axios'

class Axios {

    config(urls: string, datas: any, methods: any, token?: string) {
        return new Promise((resolve, reject) => {
            axios({
                method: methods,
                url: urls,
                timeout: 30000,
                data: datas,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Authorization': 'Bearer ' + token
                },
            })
                .then((response: any) => {
                    resolve(response)
                })
                .catch((error: any) => reject(error))
        })
    }

    method(method: string, url: string, data: any, token?: string) {
        return new Promise((resolve, reject) => {
            this.config(url, data, method, token)
                .then((result: any) => {
                    if (result.status === 200) {
                        resolve(result)
                    } else {
                        reject(result)
                    }

                })
                .catch(error => {
                    try {
                        if (error.response.data.type === 'NOT_FOUND') {
                            resolve(error)
                        }
                        reject(error)
                    } catch {
                        reject(error)
                    }
                })
        })
    }

    get(url: string, data: any, token?: string) {
        return new Promise((resolve, reject) => {
            this.config(url, data, 'GET', token)
                .then(result => {
                    resolve(result)
                })
                .catch(error => {
                    reject(error)
                })
        })
    }

    post(url: string, data: any, token?: string) {
        return new Promise((resolve, reject) => {
            this.config(url, data, 'POST', token)
                .then(result => {
                    resolve(result)
                })
                .catch(error => {
                    reject(error)
                })
        })
    }

}

export default new Axios()